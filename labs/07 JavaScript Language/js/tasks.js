// tasks.js
// This script manages a to-do list.

// Need a global variable:
var tasks = []; 

// Function called when the form is submitted.
// Function adds a task to the global array.
function addTask() {
    'use strict';

    // Get the task:
    var task = document.getElementById('task');

    // Reference to where the output goes:
    var output = document.getElementById('output');
    
    // For the output:
    var message = '';

    if (task.value) {
    
        // Add the item to the array:
        tasks.push(task.value);
        
        // Update the page:
        message = '<h2>To-Do</h2><ol>';
        for (var i = 0, count = tasks.length; i < count; i++) {
            message += '<li>' + tasks[i] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;
        
    } // End of task.value IF.

    // Return false to prevent submission:
    return false;
    
} // End of addTask() function.

function removeDuplicates() {
    'use strict';

    // Get the task:
    var task = document.getElementById('task');

    // Reference to where the output goes:
    var output = document.getElementById('output');

    // For the output:

    var message = '';

    if (output) {

        var out = [];
        var len = tasks.length - 1;
        if (len >= 0) {
            for (var i = 0; i < len; i++) {
                if (tasks[i] !== tasks[i+1]) {
                    out.push (tasks[i]);
                }
            }
            out.push (tasks[len]);
        }
        // Update the page:
        message = '<h2>testi</h2><ol>';
        for (var k = 0, count = tasks.length; k < count; i++) {
            message += '<li>' + tasks[k] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;

    } // End of task.value IF.

    // Return false to prevent submission:
    return false;

} // End of addTask() function.

// Initial setup:

    function submitForm(button) {
        'use strict';

        if (button.value === "Add It!") {
            /* open popup */
            addTask();
        } else if (button.value === "Remove Duplicates") {
            removeDuplicates();
        }
        return false;
    }

function uniq(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}

function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = submitForm;
}

window.onload = init;